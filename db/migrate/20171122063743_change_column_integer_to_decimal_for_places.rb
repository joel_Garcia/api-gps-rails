class ChangeColumnIntegerToDecimalForPlaces < ActiveRecord::Migration[5.1]
  def change
    change_column :places, :latitude, :decimal, :precision=>64, :scale=>12
    change_column :places, :longitude, :decimal, :precision=>64, :scale=>12
  end
end
