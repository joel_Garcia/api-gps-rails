class CreatePlaces < ActiveRecord::Migration[5.1]
  def change
    create_table :places do |t|
      t.integer :latitude, :limit => 8
      t.integer :longitude, :limit => 8
      t.timestamps
    end
  end
end
