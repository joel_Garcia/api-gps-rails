class Api::V1::PlacesController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def index
    place_start = Place.first
    place_end = Place.last
    render json: { start: place_start, end: place_end }.to_json, status: :ok
  end

  def create
    place = Place.new(article_params)
    if place.save
      render json: place, status: :ok
    else
      render json: place.errors, status: :unprocessable_entity
    end
  end

  private
  def article_params
    params.require(:place).permit(:latitude, :longitude)
  end
end
